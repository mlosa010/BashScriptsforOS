#!/bin/bash

declare -i num1
declare -i num2
declare -i num3
declare -i choice

echo "enter a number"
read num1
echo "enter another number"
read num2
echo "enter one last number"
read num3
declare -i sum=$(( $num1 + $num2 + $num3 ))
echo "type 1 of you would like to add these numbers, type 2 if you would like the average"
read choice

if [ $choice -eq 1 ];then
  echo "the sum of your numbers is " $sum
elif [ $choice -eq 2 ];then
  declare -i avg=$(( $sum/3 ))
  echo "the average is" $avg
else
  echo "not a valid entry"
fi
